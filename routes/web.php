<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

	Route::get('/home', 'HomeController@index')->name('home');
	Route::prefix('admin/playfair')->group(function () {


		//categories
		Route::get('getCategories',['as' => 'get.categories','uses' => 'CategoryController@getCategories']);
		Route::post('createCategory',['as' => 'create.categories','uses' => 'CategoryController@store']);
		Route::put('category/{id}',['as' => 'update.category', 'uses' => 'CategoryController@update']);
		Route::delete('category/{id}',['as' => 'destroy.category', 'uses' => 'CategoryController@destroy']);
		Route::get('getAllCategories',['as' => 'get.getAllCategories','uses' => 'CategoryController@getAllCategories']);


		//Manufacturers
		Route::get('getManufacturers',['as' => 'get.manufacturers','uses' => 'ManufacturerController@getManufacturers']);
		Route::post('createManufacturer',['as' => 'create.manufacturers','uses' => 'ManufacturerController@store']);
		Route::put('manufacturer/{id}',['as' => 'update.manufacturer', 'uses' => 'ManufacturerController@update']);
		Route::delete('manufacturer/{id}',['as' => 'destroy.manufacturer','uses' => 'ManufacturerController@destroy']);
		Route::get('getAllManufacturer',['as' => 'get.getAllManufacturer','uses' => 'ManufacturerController@getAllManufacturer']);


		//Product
		Route::get('getProducts',['as' => 'get.products','uses' => 'ProductController@getProducts']);
		Route::post('createProduct',['as' => 'create.products','uses' => 'ProductController@store']);
		Route::put('product/{id}',['as' => 'update.product', 'uses' => 'ProductController@update']);
		Route::delete('product/{id}',['as' => 'destroy.product','uses' => 'ProductController@destroy']);


		//Inventory
		Route::get('getInventory',['as' => 'get.inventories','uses' => 'InventoryController@getInventory']);
		Route::post('createInventory',['as' => 'create.inventories','uses' => 'InventoryController@createInventory']);


		//Image
		Route::post('getImages',['as' => 'get.images','uses' => 'ImageController@getImages']);
		Route::post('createImage',['as' => 'create.images','uses' => 'ImageController@store']);
		Route::put('image/{id}',['as' => 'update.images','uses' => 'ImageController@update']);
		Route::delete('image/{id}',['as' => 'delete.image','uses' => 'ImageController@destroy']);


		//Comparison
		Route::post('getComparisons',['as' => 'get.comparisons','uses' => 'ComparisonController@getComparisons']);
		Route::post('createComparison',['as' => 'create.products','uses' => 'ComparisonController@store']);
		Route::put('comparison/{id}',['as' => 'update.product', 'uses' => 'ComparisonController@update']);
		Route::delete('comparison/{id}',['as' => 'destroy.product','uses' => 'ComparisonController@destroy']);


		//Attribute
		Route::get('getAllAttributes',['as' => 'get.getAllAttributes','uses' => 'AttributeController@getAllAttributes']);
		Route::get('getAttributes',['as' => 'get.attributes','uses' => 'AttributeController@getAttributes']);
		Route::post('createAttribute',['as' => 'create.products','uses' => 'AttributeController@store']);
		Route::put('attribute/{id}',['as' => 'update.product', 'uses' => 'AttributeController@update']);
		Route::delete('attribute/{id}',['as' => 'destroy.product','uses' => 'AttributeController@destroy']);


		//ValueAttribute
		Route::post('getAttributesValues',['as' => 'get.attributeValues','uses' => 'AttributeValueController@getAttributesValues']);
		Route::post('createAttributeValue',['as' => 'create.products','uses' => 'AttributeValueController@store']);
		Route::delete('attributeValue/{id}',['as' => 'destroy.product','uses' => 'AttributeValueController@destroy']);


		//WeightUnits
		Route::get('getWeightUnits',['as' => 'getWeightUnits','uses' => 'WeightUnitController@getWeightUnits']);


		//Options
		Route::post('getOptions',['as' => 'get.option', 'uses' => 'OptionController@getOptions']);
		Route::post('createOption',['as' => 'create.option', 'uses' => 'OptionController@store']);
		Route::delete('option/{id}',['as' => 'delete.option', 'uses' => 'OptionController@destroy']);


		//Discount
		Route::post('getDiscounts',['as' => 'get.discounts', 'uses' => 'DiscountController@getDiscounts']);
		Route::post('createDiscount',['as' => 'get.discount', 'uses' => 'DiscountController@store']);
		Route::put('discount/{id}',['as' => 'update.discount', 'uses' => 'DiscountController@update']);
		Route::delete('discount/{id}',['as' => 'delete.discount', 'uses' => 'DiscountController@destroy']);


		//Coupons
	    Route::get('coupon', ['as' => 'api.coupon.index', 'uses' => 'CouponController@index']);
	    Route::post('coupon', ['as' => 'api.coupon.store', 'uses' => 'CouponController@store']);
	    Route::put('coupon/{id}', ['as' => 'api.coupon.update', 'uses' => 'CouponController@update']);
	    Route::delete('coupon/{id}', ['as' => 'api.coupon.delete', 'uses' => 'CouponController@delete']);


		//CustomersGroup
		Route::get('getCustomersGroup',['as' => 'get.categories','uses' => 'RolesController@getCustomersGroup']);
		Route::get('getAllCustomersGroup',['as' => 'get.getAllCategories','uses' => 'RolesController@getAllRols']);
		Route::post('createCustomerGroup',['as' => 'create.categories','uses' => 'RolesController@store']);
		Route::put('customerGroup/{id}',['as' => 'update.category', 'uses' => 'RolesController@update']);
		Route::delete('customerGroup/{id}',['as' => 'destroy.category', 'uses' => 'RolesController@destroy']);


		//Customer
		Route::get('getCustomers',['as' => 'get.customers','uses' => 'CustomerController@getCustomers']);
		Route::post('createCustomer',['as' => 'create.customer','uses' => 'CustomerController@store']);
		Route::put('customer/{id}',['as' => 'update.customer', 'uses' => 'CustomerController@update']);
		Route::delete('customer/{id}',['as' => 'destroy.customer','uses' => 'CustomerController@destroy']);


		//Address
		Route::post('getAddresses',['as' => 'get.addresses','uses' => 'AddressController@getAddresses']);
		Route::post('createAddress',['as' => 'store.address','uses' => 'AddressController@store']);
		Route::delete('address/{id}',['as' => 'destroy.address','uses' => 'AddressController@destroy']);


		//Taxes
		Route::get('getTaxes',['as' => 'get.taxes','uses' => 'TaxController@getTaxes']);
		Route::post('createTax',['as' => 'create.tax','uses' => 'TaxController@store']);
		Route::put('tax/{id}',['as' => 'update.tax', 'uses' => 'TaxController@update']);
		Route::delete('tax/{id}',['as' => 'destroy.tax','uses' => 'TaxController@destroy']);

		//Orders
		Route::get('getOrders',['as' => 'get.orders','uses' => 'OrderController@getOrders']);
		Route::post('createOrder',['as' => 'create.order','uses' => 'OrderController@store']);
		Route::put('order/{id}',['as' => 'update.order', 'uses' => 'OrderController@update']);
		Route::delete('order/{id}',['as' => 'destroy.order','uses' => 'OrderController@destroy']);


		//PriceSpecial
		Route::post('getPriceSpecials',['as' => 'get.price_specials','uses' => 'PriceSpecialController@getPriceSpecials']);
		Route::post('createPriceSpecial',['as' => 'create.price_special','uses' => 'PriceSpecialController@store']);
		Route::put('price_special/{id}',['as' => 'update.price_special', 'uses' => 'PriceSpecialController@update']);
		Route::delete('price_special/{id}',['as' => 'destroy.price_special','uses' => 'PriceSpecialController@destroy']);

		//Settings
	    Route::get('setting', ['as' => 'api.setting.index', 'uses' => 'SettingController@index']);
	    Route::post('setting', ['as' => 'api.setting.store', 'uses' => 'SettingController@store']);
	    Route::put('setting/{id}', ['as' => 'api.setting.update', 'uses' => 'SettingController@update']);
	    Route::delete('setting/{id}', ['as' => 'api.setting.delete', 'uses' => 'SettingController@delete']);

	});



});


// Localization
Route::get('/js/lan.js', function () {
    $strings = Cache::remember('lan.js', 0, function () {
        $lang = config('app.locale');

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

//error vue
route::get('{any}',function(){
	return view('home');
})->where('any','.*');







