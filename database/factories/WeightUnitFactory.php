<?php

use Faker\Generator as Faker;

$factory->define(App\WeightUnit::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
