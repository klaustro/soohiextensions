<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'address' =>  $faker->address,
		'address_2' => $faker->address,
		'city' => $faker->city,
		'state' => $faker->state,
		'zip_code' => $faker->postcode,
		'phone' => $faker->phoneNumber,
		'country' => $faker->country,
        'user_id' => $faker->numberBetween(1,15),
    ];
});
