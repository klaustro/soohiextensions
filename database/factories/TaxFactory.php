<?php

use Faker\Generator as Faker;

$factory->define(App\Tax::class, function (Faker $faker) {
    return [
        'zip_code' => $faker->postcode,
        'amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1000),
        'status' => $faker->boolean
    ];
});
