<?php

use Faker\Generator as Faker;

$factory->define(App\Rol::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['stilyst', 'front_end', 'guest', 'admin'])
    ];
});
