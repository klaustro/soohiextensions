<?php

use Faker\Generator as Faker;

$factory->define(App\Manufacturer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'alias' => $faker->word,
        'title' => $faker->title,
        'heading' => $faker->word,
        'description' => $faker->text($maxNbChars = 100),
        'email' => $faker->email,
        'image' => $faker->imageUrl
    ];
});
