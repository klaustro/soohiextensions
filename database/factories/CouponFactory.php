<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Coupon::class, function (Faker $faker) {
    return [
        'token' => str_random(30),
        'value' => rand(10, 50),
        'type' => $faker->randomElement(['percent', 'discount']),
        'user_id' => function(){
            return firstOrFactory(User::class);
        },
    ];
});
