<?php

use Faker\Generator as Faker;

$factory->define(App\Comparison::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'content' => $faker->text($maxNbChars = 200),
        'product_id' => $faker->numberBetween(1,50),
    ];
});
