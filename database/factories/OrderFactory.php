<?php

use App\Address;
use App\User;
use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'address_id' => $faker->numberBetween(1,20),
        'payment_id' => 1, 
        'user_id' => $faker->numberBetween(1,20),
        'status' => $faker->randomElement(['pending', 'process', 'completed'])
    ];
});
