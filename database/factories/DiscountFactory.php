<?php

use Faker\Generator as Faker;

$factory->define(App\Discount::class, function (Faker $faker) {
    return [
    	'percent' => $faker->randomDigitNotNull,
        'min_quantity' => $faker->numberBetween($min = 1, $max = 5),
        'max_quantity' => $faker->numberBetween($min = 6, $max = 10),
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'option_id' =>  $faker->numberBetween(1,50)
    ];
});
