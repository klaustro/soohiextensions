<?php

use Faker\Generator as Faker;

$factory->define(App\AttributeValue::class, function (Faker $faker) {
    return [
        'value' => $faker->word,
        'attribute_id' => $faker->numberBetween(1,50),
        'product_id' => $faker->numberBetween(1,50),
        'status' =>  $faker->boolean
    ];
});
