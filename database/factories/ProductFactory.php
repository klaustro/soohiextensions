<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
		'name' => $faker->word,
		'page_heading' => $faker->randomDigit,
		'short_description' => $faker->text($maxNbChars = 100),
		'long_description' =>  $faker->text($maxNbChars = 200),
		'meta_keywords' => $faker->word,
		'meta_description' => $faker->text($maxNbChars = 100),
		'SKU' => $faker->word,
		'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 1000),
		'length' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 500),
		'width' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 500),
		'weight' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 500),
		'weight_unit_id' =>  $faker->numberBetween(1,100),
		'min_quantity' => $faker->randomNumber($nbDigits = 2, $strict = false) ,
		'status' => $faker->boolean,
		'category_id' => $faker->numberBetween(1,20),
		'manufacturer_id' => $faker->numberBetween(1,20),
		'height' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 500),
		'main_image' => $faker->imageUrl(),
		'alias' => $faker->word,

    ];
});
