<?php

use Faker\Generator as Faker;

$factory->define(App\Option::class, function (Faker $faker) {
    return [
	    'color' => $faker->word,
		'size' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 500),
		'image' => $faker->imageUrl,
		'sku' => $faker->swiftBicNumber,
		'quantity' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 500),
		'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 1000),
		'product_id' => $faker->numberBetween(1,50),
    ];
});
