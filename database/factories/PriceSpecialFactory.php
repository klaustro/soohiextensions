<?php

use Faker\Generator as Faker;

$factory->define(App\PriceSpecial::class, function (Faker $faker) {
    return [
        'amount' => $faker->randomDigitNotNull,
        'start_date' => $faker->date(),
        'end_date' => $faker->date(),
        'option_id' =>  $faker->numberBetween(1,50)
    ];
});
