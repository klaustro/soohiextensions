<?php

use Faker\Generator as Faker;

$factory->define(App\Inventory::class, function (Faker $faker) {
    return [
        'quantity' => $faker->randomNumber(2),
        'product_id' => $faker->numberBetween(1,50),
    ];
});
