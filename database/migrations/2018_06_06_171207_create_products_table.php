<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('page_heading');
            $table->text('short_description');
            $table->text('long_description');
            $table->string('meta_keywords');
            $table->text('meta_description');
            $table->string('SKU');
            $table->decimal('price');
            $table->integer('length');
            $table->integer('height');
            $table->integer('width');
            $table->integer('weight');
            $table->string('main_image');
            $table->integer('min_quantity');
            $table->boolean('status');
            $table->string('alias');

            
            $table->unsignedInteger('weight_unit_id');
            $table->foreign('weight_unit_id')->references('id')->on('weight_units')->onDelete('cascade');


            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->unsignedInteger('manufacturer_id');
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
