<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
    	for ($i=0; $i < 10; $i++) {
            $images[$i] = $faker->image('public/storage/manufacturers');
        }

        for ($i=0; $i < 20; $i++) { 
        	factory(App\Manufacturer::class)->create([
		        'image' => str_replace('public/storage/manufacturers/', '', $images[random_int(0, 9)])
        	]);

        }
    }
}
