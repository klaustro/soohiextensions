<?php

use App\Setting;
use App\SettingGroup;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting_groups = SettingGroup::all();

        $setting_groups->map(function ($setting_group, $key) {
            factory(Setting::class, 8)->create([
                'setting_group_id' => $setting_group->id
            ]);
        });

    }
}
