<?php

use Illuminate\Database\Seeder;

class ComparisonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Comparison::class,50)->create();
    }
}
