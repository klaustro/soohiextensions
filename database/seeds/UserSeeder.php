<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\User::class)->create([
    		'name' => 'Administrator',
	        'email' => 'admin@123.com',
	        'password' => bcrypt('secret'), // secret
	        'rol_id' => '1',
    	]);

        factory(App\User::class,20)->create();
    }
}
