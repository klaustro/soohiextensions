<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {

    	for ($i=0; $i < 10; $i++) {
            $images[$i] = $faker->image('public/storage/products');
        }

        for ($i=0; $i < 50; $i++) { 
        	factory(App\Product::class)->create([
		        'main_image' => str_replace('public/storage/products/', '', $images[random_int(0, 9)])
        	]);

        }


    }
}
