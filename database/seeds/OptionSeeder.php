<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i=0; $i < 10; $i++) {
            $images[$i] = $faker->image('public/storage/options');
        }

        for ($i=0; $i < 50; $i++) { 
        	factory(App\Option::class)->create([
		        'image' => str_replace('public/storage/options/', '', $images[random_int(0, 9)])
        	]);

        }
    }
}
