<?php

use Illuminate\Database\Seeder;

class WeightUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\WeightUnit::class,100)->create();
    }
}
