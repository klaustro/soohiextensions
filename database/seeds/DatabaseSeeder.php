<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ManufacturerSeeder::class);
        $this->call(WeightUnitSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(InventorySeeder::class);
        $this->call(ImageSeeder::class);
        $this->call(ComparisonSeeder::class);
        $this->call(AttributeSeeder::class);
        $this->call(AttributeValueSeeder::class);
        $this->call(OptionSeeder::class);
        $this->call(DiscountSeeder::class);
        $this->call(AddressSeeder::class);
        $this->call(CouponSeeder::class);
        $this->call(TaxSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(PriceSpecialSeeder::class);
        $this->call(SettingGroupSeeder::class);
        $this->call(SettingSeeder::class);
    }
}
