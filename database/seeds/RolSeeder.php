<?php

use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Rol::class)->create([
    		'name' => 'admin',
    	]);

    	factory(App\Rol::class)->create([
    		'name' => 'guest',
    	]);

    	factory(App\Rol::class)->create([
    		'name' => 'front_end',
    	]);

    	factory(App\Rol::class)->create([
    		'name' => 'stilyst',
    	]);


    }
}
