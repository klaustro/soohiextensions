<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i=0; $i < 10; $i++) {
            $images[$i] = $faker->image('public/storage/products');
        }

        for ($i=0; $i < 30; $i++) { 
        	factory(App\Image::class)->create([
		        'url' => str_replace('public/storage/products/', '', $images[random_int(0, 9)])
        	]);

        }
    }
}
