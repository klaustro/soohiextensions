<?php

use Illuminate\Database\Seeder;

class PriceSpecialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\PriceSpecial::class,50)->create();
    }
}
