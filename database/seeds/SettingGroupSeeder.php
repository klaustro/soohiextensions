<?php

use App\SettingGroup;
use App\SettingTab;
use Illuminate\Database\Seeder;

class SettingGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingGroup::create([
            'setting_tab_id' => 1,
            'name' => 'Site Settings',
            'icon' => 'ti-world',
        ]);

        SettingGroup::create([
            'setting_tab_id' => 1,
            'name' => 'SEO Settings',
            'icon' => 'ti-cloud',
        ]);

        SettingGroup::create([
            'setting_tab_id' => 1,
            'name' => 'Webmaster Tags',
            'icon' => 'ti-cloud-up',
        ]);
        SettingGroup::create([
            'setting_tab_id' => 1,
            'name' => 'OpenGraph',
            'icon' => 'ti-pie-chart',
        ]);
        SettingGroup::create([
            'setting_tab_id' => 1,
            'name' => 'Webmaster Tags',
            'icon' => 'ti-cloud-up',
        ]);


        SettingGroup::create([
            'setting_tab_id' => 2,
            'name' => 'Email Notify',
            'icon' => 'ti-email',
        ]);

        SettingGroup::create([
            'setting_tab_id' => 2,
            'name' => 'Email Notifications',
            'icon' => 'ti-comment-alt',
        ]);
        SettingGroup::create([
            'setting_tab_id' => 2,
            'name' => 'Tax Rates',
            'icon' => 'ti-money',
        ]);
        SettingGroup::create([
            'setting_tab_id' => 2,
            'name' => 'Units',
            'icon' => 'ti-ruler',
        ]);
        SettingGroup::create([
            'setting_tab_id' => 2,
            'name' => 'Shipping',
            'icon' => 'ti-truck',
        ]);
        SettingGroup::create([
            'setting_tab_id' => 2,
            'name' => 'Payments',
            'icon' => 'ti-credit-card',
        ]);
    }
}
