<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = "orders";
	protected $fillable = ['address_id','user_id','payment_id','status'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function address()
    {
        return $this->belongsTo('App\Address');
    }


    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', 'like', "%$target%")
                ->orWhere('status', 'like', "%$target%")
                ->orWhereHas('user', function ($query) use($target){
                    $query->where('name','like',"%$target%");
                })
                ->orWhereHas('address', function ($query) use($target){
                    $query->where('name','like',"%$target%");
                });
        }
    }

   
}
