<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = "options";
    protected $fillable = ['color','size', 'image', 'sku', 'quantity','price','product_id'];


    public function scopeProduct($query,$id)
    {
        return $query->where('product_id',$id);
    }
    
}
