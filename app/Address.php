<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $table = "addresses";
    protected $fillable = ['user_id', 'address', 'address_2', 'city', 'state', 'zip_code', 'phone', 'country'];


    public function scopeUser($query,$id)
    {
        return $query->where('user_id',$id);
    }
}
