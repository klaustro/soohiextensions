<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    protected $table = "manufacturers";
    protected $fillable = [
    	'name','address','phone','alias','title','heading', 'description','email','image'
    ];


    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('name', 'like', "%$target%")
                ->orWhere('description', 'like', "%$target%")
                ->orWhere('id', $target);
        }
    }
}
