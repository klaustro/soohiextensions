<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceSpecial extends Model
{
	protected $table = "price_specials";
    protected $fillable = ['start_date','end_date','amount','option_id'];


    public function scopeOption($query,$id)
    {
        return $query->where('option_id',$id);
    }
}
