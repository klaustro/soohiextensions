<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table ="taxes";
    protected $fillable = ['zip_code','amount','status'];

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('zip_code', 'like', "%$target%")
                ->orWhere('amount', 'like', "%$target%")
                ->orWhere('status',  'like', "%$target%");
        }
    }
}
