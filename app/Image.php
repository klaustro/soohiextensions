<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";
    protected $fillable = ['url','product_id'];


    public function scopeProduct($query,$id)
    {
        return $query->where('product_id',$id);
    }
}
