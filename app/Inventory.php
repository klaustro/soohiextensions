<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table = "inventories";
    protected $fillable = ['quantity','product_id'];

    public function product()
    {
    	return $this->BelongsTo('App\Product','product_id','id');
    }
}
