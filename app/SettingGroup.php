<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingGroup extends Model
{
    protected $fillable = ['name'];

    //Relationships
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }

    //Scopes
    public function scopeTab($query, $tab)
    {
        if ($tab != '') {
            return $query->where('setting_tab_id', $tab);
        }
    }
}
