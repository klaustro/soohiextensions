<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = ['token', 'value', 'type',];

    /**
     * Method to search by any column
     * @param  Query $query
     * @param  string $target [description]
     * @return Query
     */
    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', $target)
                ->orWhere('value', 'like', "%$target%")
                ->orWhere('type', 'like', "%$target%")
                ->orWhere('token', 'like', "%$target%");
        }
    }
}
