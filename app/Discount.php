<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = "discounts";
    protected $fillable = ['max_quantity','min_quantity','start_date','end_date','option_id','percent'];


    public function scopeOption($query,$id)
    {
        return $query->where('option_id',$id);
    }
}
