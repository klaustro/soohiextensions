<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comparison extends Model
{
    protected $table = "comparisons";
    protected $fillable = ['title', 'content','product_id'];

    public function scopeProduct($query,$id)
    {
        return $query->where('product_id',$id);
    }

    public function product()
    {
    	return $this->belongsTo('App\Product','product_id','id');
    }
}
