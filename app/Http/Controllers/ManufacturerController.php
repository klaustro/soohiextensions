<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manufacturer;

class ManufacturerController extends Controller
{
	public function getAllManufacturer()
	{
		$manufacturer = Manufacturer::all();
		return $manufacturer;
	}

	public function getManufacturers()
	{
		$manufacturer = Manufacturer::search(request()->search)
				->orderBy('id','desc')
				->paginate(5);

		return $manufacturer;
	}

	private function loadImg()
    {
      	$Base64Img = request()->Base64Img;
		list(, $Base64Img) = explode(';', $Base64Img);
		list(, $Base64Img) = explode(',', $Base64Img);
		$Base64Img = base64_decode($Base64Img);
		$file = file_put_contents(request()->draft['image'], $Base64Img);
		rename(public_path().'/'.request()->draft['image'],public_path().'/storage/manufacturers/'.request()->draft['image']);
    }

	public function store()
	{
		$this->loadImg();
		$manufacturer = Manufacturer::create(request()->draft);

		$manufacturer->save();

		return ['message' => 'Manufacturer was created successfully', 'data' => $manufacturer];

	}

	public function update($id)
	{
		if (request()->Base64Img != null) {
			$this->loadImg();
		}
		$manufacturer = Manufacturer::find($id);
		$manufacturer->fill(request()->draft);
		$manufacturer->save();

		return ['message' => 'Manufacturer updated successfully' , 'data' => $manufacturer];
	}

	public function destroy($id)
	{
		$manufacturer = Manufacturer::find($id);
		$manufacturer->delete();

		return ['message' => 'Manufacturer deleted successfully', 'data' => $manufacturer];
	}
}
