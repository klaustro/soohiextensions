<?php

namespace App\Http\Controllers;

use App\Attribute;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function getAttributes()
    {
        $attributes = Attribute::search(request()->search)
                    ->orderBy('id','desc')
                    ->paginate(5);
        return $attributes;
    }

    public function getAllAttributes()
    {
        $attributes = Attribute::all();
        return $attributes;
    }

    public function store()
    {
        $attribute = Attribute::create(request()->all());
        $attribute->save();

        return ['message' => 'Attribute was created successfully' , 'data' => $attribute];
    }

    public function update($id)
    {
        $attribute = Attribute::find($id);
        $attribute->fill(request()->all());
        $attribute->save();

        return ['message' => 'Attribute updated successfully', 'data' => $attribute];
    }

    public function destroy($id)
    {
    	$attribute = Attribute::find($id);
    	$attribute->delete();

    	return ['message' => 'Attribute deleted successfully', 'data' => $attribute];
    }
}
