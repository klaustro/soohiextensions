<?php

namespace App\Http\Controllers;

use App\AttributeValue;
use App\Image;
use App\Option;
use App\Product;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function getProducts()
    {
        $products = Product::with('categories','manufacturer','inventory')
                    ->search(request()->search)
                    ->orderBy('id','desc')
                    ->paginate(5);

        return $products; 
    }

    private function loadImg()
    {
        $Base64Img = request()->draft['base64'];
        list(, $Base64Img) = explode(';', $Base64Img);
        list(, $Base64Img) = explode(',', $Base64Img);
        $Base64Img = base64_decode($Base64Img);
        $file = file_put_contents(request()->draft['main_image'], $Base64Img);
        rename(public_path().'/'.request()->draft['main_image'],public_path().'/storage/products/'.request()->draft['main_image']);
    }

 
  
    public function store()
    {
        $result = null;
        DB::transaction(function() use(&$result) {

            $this->loadImg();
            $product = Product::create(request()->draft);
            $product->save();

            foreach(request()->images as  $image ) {
                $Base64Img = $image['Base64Img'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents($image['url'], $Base64Img);
                rename(public_path().'/'.$image['url'],public_path().'/storage/products/'.$image['url']);

                $images = Image::create([
                    'url' => $image['url'],
                    'product_id' => $product->id
                ]);
                $images->save();
            } 

            foreach(request()->arr as  $values) {
                $value = AttributeValue::create([
                    'value' => $values['value'],
                    'attribute_id' => $values['attribute_id'],
                    'status' => $values['status'],
                    'product_id' => $product->id
                ]);
                $value->save();
            }


            foreach(request()->arrOptions as $valueOption){
                $Base64Img = $valueOption['base64'];
                list(, $Base64Img) = explode(';', $Base64Img);
                list(, $Base64Img) = explode(',', $Base64Img);
                $Base64Img = base64_decode($Base64Img);
                $file = file_put_contents($valueOption['image'], $Base64Img);
                rename(public_path().'/'.$valueOption['image'],public_path().'/storage/options/'.$valueOption['image']);

                $option =   Option::create([
                    'color' => $valueOption['color'],
                    'size' => $valueOption['size'],
                    'image' => $valueOption['image'],
                    'sku' => $valueOption['SKU'],
                    'quantity' => $valueOption['quantity'],
                    'price' => $valueOption['price'],
                    'product_id' => $product->id
                ]);
                $option->save();
            }

            $result = $product;
        });

        return ['message' => 'Product was created successfully' , 'data' => $result];
    }

   
    public function update($id)
    {
        if (request()->Base64Img != null) {
            $this->loadImg();
        }
        $product = Product::find($id);
        $product->fill(request()->draft);
        $product->save();

        $result = Product::with('categories','manufacturer','inventory')->find($product->id);
        return ['message' => 'Product updated successfully' , 'data' => $result];

    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return ['message' => 'Product deleted successfully','data' => $product];
    }
}
