<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function getAddresses()
    {
        $address = Address::user(request()->id)->get();;
        return $address;
    }

    public function store()
    {
        foreach(request()->draft as $value){
            $address = Address::create([
                'name' => $value['name'],
                'user_id' => request()->id
            ]);
            $address->save();
        }
        return ['message' => 'Address was created successfuly', 'data' => $address];
    }


    public function destroy($id)
    {
        $address = Address::find($id);
        $address->delete();

        return ['message' => 'Address was deleted successfuly','data' => $address];
    }
}
