<?php

namespace App\Http\Controllers;

use App\Inventory;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
	public function getInventory()
	{
		$inventory = Inventory::with('product')->get();
		return $inventory;
	}


	public function createInventory()
	{
		$inventory = Inventory::create(request()->draft);
		$inventory->save();
		
        $result = Inventory::with('product')->find($inventory->id);

		return ['message' => 'Inventory was created successfully' , 'data' => $result];
	}
}
