<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function getOrders()
    {
        $orders = Order::with('user','address')
                        ->search(request()->search)
                        ->orderBy('id','desc')
                        ->paginate(5);
        return $orders;
    }

    public function store()
    {
        $order = Order::create(request()->draft);
        $order->save();

        return ['message' => 'Order created successfuly' , 'data' => $order];
    }

    public function update($id)
    {
        $order = Order::find($id);
        $order->fill(request()->all());
        $order->save();

        return ['message' => 'Order updated successfuly' , 'data' => $order];
    }

    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();

        return ['message' => 'Order deleted successfuly' , 'data' => $order];
    }
}
