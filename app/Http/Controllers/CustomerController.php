<?php

namespace App\Http\Controllers;

use App\Address;
use App\User;
use Illuminate\Http\Request;
use DB;

class CustomerController extends Controller
{
    public function getCustomers()
    {
    	$customers = User::with('rol','addresses')->search(request()->search)
				->orderBy('id','desc')
				->paginate(5);

    	return $customers;
    }

    public function store()
    {
    	$result = null;
    	DB::transaction(function() use(&$result) {
			$customer = User::create([
                'name' => request()->draft['name'],
                'email' => request()->draft['email'],
                'password' => bcrypt(request()->draft['password']),
                'rol_id' => request()->draft['rol_id'],
            ]);
	    	$customer->save();

	    	foreach(request()->arr as $value){
	    		$address = Address::create([
		    		'name' => $value['name'],
		    		'user_id' => $customer->id
	    		]);
	    	}
	    	$address->save();

	    	$result = User::with('rol','addresses')->find($customer->id);
    	});

    	
    	return ['message' => 'Customer was created successfuly', 'data' => $result];
    }

    public function update($id)
    {
    	$customer = User::find($id);
    	$customer->name = request()->draft['name'];
        $customer->email = request()->draft['email'];
        $customer->password = bcrypt(request()->draft['password']);
        $customer->rol_id = request()->draft['rol_id'];
    	$customer->save();

    	$result = User::with('rol')->find($customer->id);

    	return ['message' => 'Customer was updated successfuly', 'data' => $result];
    }

    public function destroy($id)
    {
    	$customer = User::find($id);
    	$customer->delete();

    	return ['message' => 'Customer was deleted successfuly', 'data' => $customer ];
    }
}
