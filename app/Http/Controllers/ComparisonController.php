<?php

namespace App\Http\Controllers;

use App\Comparison;
use Illuminate\Http\Request;

class ComparisonController extends Controller
{
    public function getComparisons()
    {
        $comparisons = Comparison::with('product')->product(request()->id)->get();
        return $comparisons;
    }

    public function store()
    {
        $comparison = Comparison::create([
            'title' => request()->data['title'],
            'content' => request()->data['content'],
            'product_id' => request()->id
        ]);
        $comparison->save();

        return ['message' => 'Comparison was created successfully', 'data' => $comparison];
    }


    public function update($id)
    {
        $comparison = Comparison::find($id);
        $comparison->fill(request()->all());
        $comparison->save();

        return ['message' => 'Comparison updated successfully' , 'data' => $comparison];

    }

    public function destroy($id)
    {
        $comparison = Comparison::find($id);
        $comparison->delete();

        return ['message' => 'Comparison deleted successfully' , 'data' => $comparison];
    }
}
