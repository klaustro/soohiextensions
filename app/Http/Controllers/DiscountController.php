<?php

namespace App\Http\Controllers;

use App\Discount;
use Illuminate\Http\Request;

class DiscountController extends Controller
{

    public function getDiscounts()
    {
        $discounts = Discount::option(request()->id)->get();
        return $discounts;
    }
    
    public function store()
    {
        $discount = Discount::create([
            'percent' => request()->draft['percentDiscount'],
            'min_quantity' => request()->draft['min_quantityDiscount'],
            'max_quantity' => request()->draft['max_quantityDiscount'],
            'start_date' => request()->draft['start_dateDiscount'],
            'end_date' => request()->draft['end_dateDiscount'],
            'option_id' => request()->option_id
        ]);
        $discount->save();

        return ['message' => 'Discount was created successfully', 'data' => $discount];
    }

   
    public function update($id)
    {
        $discount = Discount::find($id);
        $discount->fill(request()->all());
        $discount->save();

        return ['message' => 'Discount updated successfully', 'data' => $discount];
    }

    public function destroy($id)
    {
        $discount = Discount::find($id);
        $discount->delete();

        return ['message' => 'Discount deleted successfully', 'data' => $discount];
    }
}
