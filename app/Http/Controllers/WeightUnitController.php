<?php

namespace App\Http\Controllers;

use App\WeightUnit;
use Illuminate\Http\Request;

class WeightUnitController extends Controller
{
    public function getWeightUnits()
    {
        $getWeightUnits = WeightUnit::all();
        return $getWeightUnits;
    }
}
