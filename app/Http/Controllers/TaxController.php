<?php

namespace App\Http\Controllers;

use App\Tax;
use Illuminate\Http\Request;

class TaxController extends Controller
{
    public function getTaxes()
    {
        $taxes = Tax::search(request()->search)
                    ->orderBy('id','desc')
                    ->paginate(5);

        return $taxes;
    } 

    public function store()
    {
        $tax = Tax::create(request()->draft);
        $tax->save();

        return ['message' => 'Tax registered successfully', 'data' => $tax];
    }

    public function update($id)
    {
        $tax = Tax::find($id);
        $tax->fill(request()->all());
        $tax->save();

        return ['message' => 'Tax updated successfully', 'data' => $tax];
    }

    public function destroy($id)
    {
        $tax = Tax::find($id);
        $tax->delete();

        return ['message' => 'Tax deleted successfully', 'data' => $tax];
    }
}
