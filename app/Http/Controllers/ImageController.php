<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{

    public function getImages()
    {
        $image = Image::product(request()->id)->get();
        return $image;
    }

    public function store()
    {

        $data = [];
        foreach(request()->image as $key => $image ) {

            $Base64Img = $image['Base64Img'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents($image['url'], $Base64Img);
            rename(public_path().'/'.$image['url'],public_path().'/storage/products/'.$image['url']);

            $images = Image::create([
                'url' => $image['url'],
                'product_id' => request()->id
            ]);
            $images->save();

            $data[$key]['id'] = $images->id;
            $data[$key]['url'] = $images->url;
        }   

        return ['message' => 'Images was created successfully', 'data' => $data];
    }

    public function update($id)
    {
     
        $Base64Img = request()->Base64Img;
        list(, $Base64Img) = explode(';', $Base64Img);
        list(, $Base64Img) = explode(',', $Base64Img);
        $Base64Img = base64_decode($Base64Img);
        $file = file_put_contents(request()->draft['url'], $Base64Img);
        rename(public_path().'/'.request()->draft['url'],public_path().'/storage/products/'.request()->draft['url']);

        $image = Image::find($id);
        $image->fill(request()->draft);
        $image->save();

        return ['message' => 'Image updated successfully' , 'data' => $image];
    }

    public function destroy($id)
    {
        $image = Image::find($id);
        $image->delete();

        return ['message' => 'Image deleted successfully', 'data' => $image];
    }
}
