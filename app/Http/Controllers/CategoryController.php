<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CategoryController extends Controller
{

	public function getAllCategories()
	{
		$categories = Category::all();
		return $categories;
	}

	public function getCategories()
	{
		$categories = Category::search(request()->search)
					->orderBy('id','desc')
					->paginate(5);
					
		return $categories;
	}

	public function store()
	{
		$category = Category::create(request()->all());
		$category->save();

 		Mail::to(Auth()->user()->email)->send(new Test($category));

		return ['message' => 'Category was created successfully', 'data' => $category];
	}

	public function update($id)
	{
		$category = Category::find($id);
		$category->fill(request()->all());
		$category->save();

		return ['message' => 'Category updated successfully' , 'data' => $category];
	}

	public function destroy($id)
	{
		$category = Category::find($id);
		$category->delete();

		return ['message' => 'Category deleted successfully', 'data' => $category];
	}
}
