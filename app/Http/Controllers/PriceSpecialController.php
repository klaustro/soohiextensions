<?php

namespace App\Http\Controllers;

use App\PriceSpecial;
use Illuminate\Http\Request;

class PriceSpecialController extends Controller
{
    public function getPriceSpecials()
    {
        $PriceSpecials = PriceSpecial::option(request()->id)->get();
        return $PriceSpecials;
    }
    
    public function store()
    {
        $PriceSpecial = PriceSpecial::create([
            'amount' => request()->draft['amountPriceSpecial'],
            'start_date' => request()->draft['start_datePriceSpecial'],
            'end_date' => request()->draft['end_datePriceSpecial'],
            'option_id' => request()->option_id
        ]);
        $PriceSpecial->save();

        return ['message' => 'Price special was created successfully', 'data' => $PriceSpecial];
    }

   
    public function update($id)
    {
        $PriceSpecial = PriceSpecial::find($id);
        $PriceSpecial->fill(request()->all());
        $PriceSpecial->save();

        return ['message' => 'Price special updated successfully', 'data' => $PriceSpecial];
    }

    public function destroy($id)
    {
        $PriceSpecial = PriceSpecial::find($id);
        $PriceSpecial->delete();

        return ['message' => 'Price special deleted successfully', 'data' => $PriceSpecial];
    }
}
