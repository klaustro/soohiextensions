<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function index()
    {
        return Coupon::search(request()->search)
            ->orderBy(request()->orderBy, request()->desc == 'true' ? 'DESC' : 'ASC')
            ->paginate(5);
    }

    public function store()
    {
        $coupon = new Coupon();

        do {
            $coupon->token = str_random(30);
        } while  (Coupon::where('token', '=', $coupon->token)->first() instanceof Coupon);

        $coupon->value = request()->value;
        $coupon->type = request()->type;
        $coupon->save();

        return [
            'message' => trans('app.coupon.store_message'),
            'id' => $coupon->id,
            'token' => $coupon->token,
        ];
    }

    public function update($id)
    {
        $coupon = Coupon::find($id);
        $coupon->fill(request()->all());
        $coupon->save();

        return ['message' => trans('app.coupon.update_message')];
    }

    public function delete($id)
    {
        $coupon = Coupon::destroy($id);

        return ['message' => trans('app.coupon.delete_message')];
    }

    public function list()
    {
        return Coupon::all();
    }

}
