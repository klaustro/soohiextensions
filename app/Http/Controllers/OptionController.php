<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public function getOptions()
    {
        $option = Option::product(request()->id)->get();
        return $option;
    }

    public function store()
    {
        $data = [];
        foreach(request()->draft as $key => $value){

            $Base64Img =  $value['base64'];
            list(, $Base64Img) = explode(';', $Base64Img);
            list(, $Base64Img) = explode(',', $Base64Img);
            $Base64Img = base64_decode($Base64Img);
            $file = file_put_contents($value['image'], $Base64Img);
            rename(public_path().'/'.$value['image'],public_path().'/storage/options/'.$value['image']);

            $option = Option::create([
                'color' =>  $value['color'],
                'size' =>  $value['size'],
                'image' =>  $value['image'],
                'sku' =>  $value['SKU'],
                'quantity' =>  $value['quantity'],
                'price' =>  $value['price'],
                'product_id' =>  request()->id
            ]);

        $option->save();

            $data[$key]['id'] = $option->id;
            $data[$key]['color'] = $option->color;
            $data[$key]['size'] = $option->size;
            $data[$key]['image'] = $option->image;
            $data[$key]['sku'] = $option->sku;
            $data[$key]['quantity'] = $option->quantity;
            $data[$key]['price'] = $option->price;


        }
      

        return ['message' => 'Option was created successfully', 'data' => $data];

    }


  
    public function destroy($id)
    {
        $option = Option::find($id);
        $option->delete();

        return ['message' => 'Option deleted successfully', 'data' => $option];
    }
}
