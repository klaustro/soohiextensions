<?php

namespace App\Http\Controllers;

use App\Rol;
use Illuminate\Http\Request;

class RolesController extends Controller
{

    public function getCustomersGroup()
    {
        $rols = Rol::search(request()->search)
                ->orderBy('id', 'desc')
                ->paginate(5);
        return $rols;
    }


    public function getAllRols()
    {
        $rols = Rol::all();
        return $rols;
    }


    public function store()
    {
        $rol = Rol::create(request()->all());
        $rol->save();
        return ['message' => 'Rol was created successfuly' ,'data' => $rol];
    }


    public function update($id)
    {
        $rol = Rol::find($id);
        $rol->fill(request()->draft);
        $rol->save();

        return ['message' => 'Rol was updated successfuly' ,'data' => $rol];
    }


    public function destroy($id)
    {
        $rol = Rol::find($id);
        $rol->delete();

        return ['message' => 'Rol was deleted successfuly' ,'data' => $rol];
    }
}
