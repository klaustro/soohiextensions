<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\AttributeValue;
use Illuminate\Http\Request;
use DB;

class AttributeValueController extends Controller
{

    public function getAttributesValues()
    {
        $value = AttributeValue::with('products','attributes')->product(request()->id)
                ->get();
        return $value;
    }

    public function store()
    {
        $data = [];
        foreach(request()->draft as $key => $values) {
            $value = AttributeValue::create([
                'value' => $values['value'],
                'attribute_id' => $values['attribute_id'],
                'product_id' => request()->id,
                'status' => $values['status']
            ]);
            $value->save();

            $data[$key]['id'] = $value->id;
            $data[$key]['value'] = $value->value;
            $data[$key]['attribute_id'] = $value->attribute_id;
            $data[$key]['status'] = $value->status;

        }

        $result = AttributeValue::with('products','attributes')->find($value->id);

        return ['message' => 'Attribute was created successfully' , 'data' =>  $data, 'result' => $result];
    }

    public function destroy($id)
    {
        $value = AttributeValue::find($id);
        $value->delete();

        return ['message' => 'Attribute deleted successfully' , 'data' => $value];
    }
}
