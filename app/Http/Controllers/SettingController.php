<?php

namespace App\Http\Controllers;

use App\Setting;
use App\SettingGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SettingController extends Controller
{
    public function index()
    {
        Log::info(request()->all());
        return SettingGroup::with('settings')->tab(request()->tab)->get();
    }

    public function store()
    {
        $setting = new Setting();
        $setting->setting_group_id = request()->setting_group_id;
        $setting->param = request()->param;
        $setting->value = request()->value;
        $setting->save();

        return [
            'message' => trans('app.setting.store_message'),
            'id' => $setting->id,
        ];
    }

    public function update($id)
    {
        $setting = Setting::find($id);
        $setting->fill(request()->all());
        $setting->save();

        return ['message' => trans('app.setting.update_message')];
    }

    public function delete($id)
    {
        $setting = Setting::destroy($id);

        return ['message' => trans('app.setting.delete_message')];
    }
}
