<?php

namespace App\Http\Controllers;

use App\Stilist;
use Illuminate\Http\Request;

class StilistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stilist  $stilist
     * @return \Illuminate\Http\Response
     */
    public function show(Stilist $stilist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stilist  $stilist
     * @return \Illuminate\Http\Response
     */
    public function edit(Stilist $stilist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stilist  $stilist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stilist $stilist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stilist  $stilist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stilist $stilist)
    {
        //
    }
}
