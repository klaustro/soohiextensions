<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    protected $fillable = [
	  	'name', 'SKU', 'price', 'length', 'width', 'weight', 'weight_unit_id', 'min_quantity', 'status','category_id', 'manufacturer_id', 'page_heading', 'short_description', 'long_description', 'meta_keywords',  'meta_description','main_image','height','alias'
    ];
       
    public function categories()
    {
    	return $this->BelongsTo('App\Category','category_id','id');
    }

    public function manufacturer()
    {
        return $this->BelongsTo('App\Manufacturer','manufacturer_id','id');
    }

    public function inventory()
    {
        return $this->HasMany('App\Inventory','product_id','id');
    }

    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('id', 'like', "%$target%")
                ->orWhere('name', 'like', "%$target%")
                ->orWhereHas('categories', function ($query) use($target){
                    $query->where('categories.name','like',"%$target%");
                });
        }
    }


}
