<?php

if (! function_exists('firstOrFactory')) {

    /**
     * Get first model, otherwise factory one up
     *
     * @param string $modelClass
     */
    function firstOrFactory(string $modelClass)
    {
        return $modelClass::first() ?? factory($modelClass)->create();
    }
}
