<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $table = "attribute_values";
    protected $fillable = ['value','attribute_id','product_id','status'];

    public function products()
    {
    	return $this->belongsTo('App\Product','product_id','id');
    }

    public function attributes()
    {
    	return $this->belongsTo('App\Attribute','attribute_id','id');
    }

    public function scopeProduct($query,$id)
    {
        return $query->where('product_id',$id);
    }

        public function scopeAttribute($query,$id)
    {
        return $query->where('attribute_id',$id);
    }


    public function scopeSearch($query, $target)
    {
        if ($target != '') {
            return $query->
                where('value', 'like', "%$target%")
                ->orWhere('id', $target);
        }
    }

}
