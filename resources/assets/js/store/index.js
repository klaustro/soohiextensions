import Vue from 'vue'
import Vuex from 'vuex'
import Category from './modules/Category.js'
import Manufacturer from './modules/Manufacturer.js'
import Product from './modules/Product.js'
import Inventory from './modules/Inventory.js'
import Image from './modules/Image.js'
import Comparison from './modules/Comparison.js'
import Attribute from './modules/Attribute.js'
import AttributeValue from './modules/AttributeValues.js'
import WeightUnit from './modules/WeightUnit.js'
import Option from './modules/Option.js'
import Discount from './modules/Discount.js'
import PriceSpecial from './modules/PriceSpecial.js'
import CustomerGroup from './modules/CustomersGroup.js'
import Customer from './modules/Customers.js'
import Address from './modules/Address.js'
import Coupon from './modules/Coupon.js'
import Tax from './modules/Tax.js'
import Order from './modules/Order.js'
import Setting from './modules/Setting.js'

Vue.use(Vuex)

let store = new Vuex.Store({
	modules:{
		Category,
		Manufacturer,
		Product,
		Inventory,
		Image,
		Comparison,
		Attribute,
		AttributeValue,
		WeightUnit,
		Option,
		Discount,
		CustomerGroup,
		Customer,
		Address,
		Coupon,
		Tax,
		Order,
		PriceSpecial,
		Setting,
	}
})

export default store