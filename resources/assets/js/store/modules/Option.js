export default {
	state:{
		options:[],

	},
	actions:{
		getOptions( context ,id){
			axios.post('admin/playfair/getOptions/',id)
			.then(response => {
				context.commit('getOptions' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createOption(context,payload){
			axios.post('/admin/playfair/createOption',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createOption', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeOption(context, id){
			axios.delete('/admin/playfair/option/'+id)
			.then(response => {
                context.commit('removeOption', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		
	},
	mutations:{
		getOptions(state, { list }){
			state.options = list
		},
		createOption(state,newOption){
			newOption.map(option => {
   				state.options.push(option);
			})
		},
		removeOption(state, id){
			let index = state.options.findIndex(option => option.id == id)
			state.options.splice(index,1)
		}		
	},
	
}