export default {
	state:{
		customersGroup:[],
		allCustomersGroup:[],
		perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getCustomersGroup( context ,params){
			axios.get('admin/playfair/getCustomersGroup?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getCustomersGroup' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		getAllCustomersGroup( context ,params){
			axios.get('admin/playfair/getAllCustomersGroup')
			.then(response => {
				context.commit('getCustomersGroup' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createCustomerGroup(context,payload){
			axios.post('/admin/playfair/createCustomerGroup',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createCustomerGroup', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeCustomerGroup(context, id){
			axios.delete('/admin/playfair/customerGroup/'+id)
			.then(response => {
                context.commit('removeCustomerGroup', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateCustomerGroup(context, payload){
	        axios.put('/admin/playfair/customerGroup/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateCustomerGroup', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	}
	},
	mutations:{
		getCustomersGroup(state,{ list }){
			state.allCustomersGroup = list
			state.customersGroup = list.data
			state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createCustomerGroup(state,draft){
       		state.customersGroup.unshift(draft);
		},
		removeCustomerGroup(state, id){
			let index = state.customersGroup.findIndex(customerGroup => customerGroup.id == id)
			state.customersGroup.splice(index,1)
		},
		updateCustomerGroup(state, {id,draft}){
        	let index = state.customersGroup.findIndex(customerGroup => customerGroup.id == id);
        	state.customersGroup.splice(index, 1, draft);
		}
	},
	
}