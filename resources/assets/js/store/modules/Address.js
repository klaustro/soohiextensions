export default {
	state:{
		addresses:[],
	},
	actions:{
		getAddresses( context , payload){
			axios.post('admin/playfair/getAddresses',payload)
			.then(response => {
				context.commit('getAddresses' , { list: response.data })
			}).catch(error => {
				Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		createAddress( context , payload){
			axios.post('admin/playfair/createAddress',payload)
			.then(response => {
				context.commit('createAddress' ,response.data.data)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
			}).catch(error => {
				Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			
			})
		},
		removeAddress(context,id){
			axios.delete('admin/playfair/address/'+id)
			.then(response => {
				context.commit('removeAddress', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})

			})
		}
	},
	mutations:{
		createAddress(state,draft){
			state.addresses.push(draft)
		},
		getAddresses(state,{ list }){
			state.addresses = list
		},
		removeAddress(state, id){
			let index = state.addresses.findIndex(Address => Address.id == id)
			state.addresses.splice(index,1)
		},
	},
	
}