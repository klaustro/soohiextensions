let state = {
    settings: [],
}

let getters = {
        findSetting(state){
            return function(id){
                let setting = state.settings.find(setting => setting.id == id)
                return setting;
            }
        },
}

let actions = {
    getSettings(context, params){
        axios.get('admin/playfair/setting?tab=' + params.tab)
            .then(response => {
                context.commit('getSettings', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeSetting(context, payload){
        axios.post('admin/playfair/setting', payload)
            .then(response => {
                let newSetting = {
                    id: response.data.id,
                    setting_group_id: payload.setting_group_id,
                    param: payload.param,
                    value: payload.value,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                //context.commit('storeSetting', newSetting)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    updateSetting(context, payload){
        axios.put('admin/playfair/setting/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                //context.commit('updateSetting', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    removeSetting(context, id){
        axios.delete('admin/playfair/setting/' + id)
            .then(response => {
                context.commit('removeSetting', id)
                Vue.toasted.show(response.data.message, {icon: 'trash', type: 'error'})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listSettings(context){
        axios.get('admin/playfair/settingList')
            .then(response => {
                context.commit('listSettings', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getSettings(state, {data}){
        state.settings = data;
    },

    storeSetting(state, newSetting){
        state.settings.unshift(newSetting);
    },

    updateSetting(state, {id, draft}){
        let index = state.settings.findIndex(setting => setting.id == id);
        state.settings.splice(index, 1, draft);
    },

    removeSetting(state, id)    {
        let index = state.settings.findIndex(setting => setting.id == id);
        state.settings.splice(index, 1);
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}