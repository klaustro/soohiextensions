export default {
	state:{
		orders:[],
		perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getOrders( context , params){
			axios.get('admin/playfair/getOrders?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getOrders' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createOrder(context,payload){
			axios.post('/admin/playfair/createOrder',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
				context.commit('createOrder', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeOrder(context, id){
			axios.delete('/admin/playfair/order/'+id)
			.then(response => {
                context.commit('removeOrder', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateOrder(context, payload){
	        axios.put('/admin/playfair/order/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateOrder', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getOrders(state,{ list }){
			state.orders = list.data
			state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createOrder(state,newAttribute){
       		state.orders.unshift(newAttribute);
		},
		removeOrder(state, id){
			let index = state.orders.findIndex(tax => tax.id == id)
			state.orders.splice(index,1)
		},
		updateOrder(state, {id,draft}){
        	let index = state.orders.findIndex(tax => tax.id == id);
        	state.orders.splice(index, 1, draft);
		}
	},
	
}