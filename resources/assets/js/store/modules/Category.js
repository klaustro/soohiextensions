export default {
	state:{
		categories:[],
		allCategories:[],
	    perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getAllCategories({commit}){
			axios.get('admin/playfair/getAllCategories')
			.then(response => {
				commit('getAllCategories' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
			
		},
		loadCategory( context ,params){
			axios.get('admin/playfair/getCategories?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('setCategory' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createCategory(context,payload){
			axios.post('/admin/playfair/createCategory',payload)
			.then(response => {
				let newCategory = {
					id: response.data.data.id,
                    name: payload.name,
                    description:payload.description
				}
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createCategory', newCategory)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeCategory(context, id){
			axios.delete('/admin/playfair/category/'+id)
			.then(response => {
                context.commit('removeCategory', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateCategory(context, payload){
	        axios.put('/admin/playfair/category/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateCategory', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getAllCategories(state, { list }){
			return state.allCategories = list
		},
		setCategory(state,{ list }){
			state.categories = list.data
		  	state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createCategory(state,newCategory){
       		state.categories.unshift(newCategory);
		},
		removeCategory(state, id){
			let index = state.categories.findIndex(category => category.id == id)
			state.categories.splice(index,1)
		},
		updateCategory(state, {id,draft}){
        	let index = state.categories.findIndex(category => category.id == id);
        	state.categories.splice(index, 1, draft);
		}

		
	},
	
}