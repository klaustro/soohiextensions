export default {
	state:{
		weight_units:[],
	},
	actions:{
		getWeightUnits({commit}){
			axios.get('admin/playfair/getWeightUnits')
			.then(response => {
				commit('getWeightUnits' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})	
		},
	},
	mutations:{
		getWeightUnits(state, { list }){
			return state.weight_units = list
		},		
	}
	
}