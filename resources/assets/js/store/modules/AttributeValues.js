export default {
	state:{
		attributeValues:[],
		attributeInProducts:[],
		perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getAttributeValuesInProduct( context ,payload){
			axios.post('admin/playfair/getAttributesValues',payload)
			.then(response => {
				context.commit('getAttributeValues' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createAttributeValue(context,payload){
			axios.post('/admin/playfair/createAttributeValue',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createAttributeValue', response.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeAttributeValue(context, id){
			axios.delete('/admin/playfair/attributeValue/'+id)
			.then(response => {
                context.commit('removeAttributeValue', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateAttributeValue(context, payload){
	        axios.put('/admin/playfair/attributeValue/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateAttributeValue', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	}
	},
	mutations:{
		getAttributeValues(state,{ list }){
			state.attributeInProducts = list
			state.attributeValues = list.data
			state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createAttributeValue(state,draft){
			draft.data.map(value => {
       			state.attributeInProducts.unshift(value);
			})
		},
		removeAttributeValue(state, id){
			let index = state.attributeInProducts.findIndex(attributeValue => attributeValue.id == id)
			state.attributeInProducts.splice(index,1)
		},
		updateAttributeValue(state, {id,draft}){
        	let index = state.attributeInProducts.findIndex(attributeValue => attributeValue.id == id);
        	state.attributeInProducts.splice(index, 1, draft);
		}
	},
	
}