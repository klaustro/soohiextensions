export default {
	state:{
		products:[],
		// id:'',
	    perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getProducts( context ,params){
			axios.get('admin/playfair/getProducts?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getProducts' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createProduct(context,payload){
			axios.post('/admin/playfair/createProduct',payload)
			.then(response => {
				let newProduct = {
					id: response.data.data.id,
					SKU: response.data.data.SKU,
					categories:response.data.data.categories,
					height: response.data.data.height,
					inventory:response.data.data.inventory,
					length:response.data.data.length,
					long_description:response.data.data.long_description,
					main_image:response.data.data.main_image,
					manufacturer:response.data.data.manufacturer,
					manufacturer_id:response.data.data.manufacturer_id,
					meta_description:response.data.data.meta_description,
					meta_keywords:response.data.data.meta_keywords,
					min_quantity:response.data.data.min_quantity,
					name:response.data.data.name,
					page_heading:response.data.data.page_heading,
					price:response.data.data.price,
					short_description:response.data.data.short_description,
					status:response.data.data.status,
					weight:response.data.data.weight,
					weight_unit:response.data.data.weight_unit,
					width:response.data.data.width,
				}
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createProduct', newProduct)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeProduct(context, id){
			axios.delete('/admin/playfair/product/'+id)
			.then(response => {
                context.commit('removeProduct', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateProduct(context, payload){
	        axios.put('/admin/playfair/product/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateProduct', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getProducts(state,{ list }){
			state.products = list.data
		  	state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createProduct(state,newProduct){
			// state.id = newProduct.id
       		state.products.unshift(newProduct);
		},
		removeProduct(state, id){
			let index = state.products.findIndex(product => product.id == id)
			state.products.splice(index,1)
		},

		updateProduct(state, {id,draft}){
        	let index = state.products.findIndex(product => product.id == id);
        	state.products.splice(index, 1, draft);
		}

		
	},
	
}