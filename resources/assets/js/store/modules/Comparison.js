export default {
	state:{
		comparisons:[],
	},
	actions:{
		getComparisons( context ,payload){
			axios.post('admin/playfair/getComparisons',payload)
			.then(response => {
				context.commit('getComparisons' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createComparison(context,payload){
			axios.post('/admin/playfair/createComparison',payload)
			.then(response => {
				let newComparison = {
					id: response.data.data.id,
                    title: payload.data.title,
                    content:payload.data.content,
                    product_id:payload.id
				}
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createComparison', newComparison)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeComparison(context, id){
			axios.delete('/admin/playfair/comparison/'+id)
			.then(response => {
                context.commit('removeComparison', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateComparison(context, payload){
	        axios.put('/admin/playfair/comparison/' + payload.data.id, payload.data)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateComparison', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getComparisons(state,{ list }){
			state.comparisons = list
		},
		createComparison(state,newComparison){
       		state.comparisons.unshift(newComparison);
		},
		removeComparison(state, id){
			let index = state.comparisons.findIndex(comparison => comparison.id == id)
			state.comparisons.splice(index,1)
		},
		updateComparison(state, {id,data}){
        	let index = state.comparisons.findIndex(comparison => comparison.id == data.id);
        	state.comparisons.splice(index, 1, data);
		}
	},
	
}