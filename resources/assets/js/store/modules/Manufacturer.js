export default {
	state:{
		allManufacturers:[],
		manufacturers:[],
	    perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getAllManufacturers(context){
			axios.get('admin/playfair/getAllManufacturer')
			.then(response => {
				context.commit('getAllManufacturer' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		getManufacturers( context ,params){
			axios.get('admin/playfair/getManufacturers?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getManufacturers' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createManufacturer(context,payload){
			axios.post('/admin/playfair/createManufacturer',payload)
			.then(response => {
				let newManufacturer = {
					id: response.data.data.id,
                    name: response.data.data.name,
                    description:response.data.data.description,
                    address:response.data.data.address,
                    alias:response.data.data.alias,
                    email:response.data.data.email,
                    heading:response.data.data.heading,
                    phone:response.data.data.phone,
                    title:response.data.data.title,
                    image:response.data.data.image,
				}
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createManufacturer', newManufacturer)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeManufacturer(context, id){
			axios.delete('/admin/playfair/manufacturer/'+id)
			.then(response => {
                context.commit('removeManufacturer', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateManufacturer(context, payload){
	        axios.put('/admin/playfair/manufacturer/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateManufacturer', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	getters:{
		allManufacturers: state => {
			return state.allManufacturers.filter(manufacturer => manufacturer)
		}
	},
	mutations:{
		getAllManufacturer(state,{ list }){
			return state.allManufacturers = list
		},
		getManufacturers(state,{ list }){
			state.manufacturers = list.data
		  	state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createManufacturer(state,newManufacturer){
       		state.manufacturers.unshift(newManufacturer);
		},
		removeManufacturer(state, id){
			let index = state.manufacturers.findIndex(manufacturer => manufacturer.id == id)
			state.manufacturers.splice(index,1)
		},

		updateManufacturer(state, {id,draft,Base64Img}){
        	let index = state.manufacturers.findIndex(manufacturer => manufacturer.id == id);
        	state.manufacturers.splice(index, 1, draft);
		}

		
	},
	
}