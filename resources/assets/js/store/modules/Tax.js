export default {
	state:{
		taxes:[],
		perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getTaxes( context , params){
			axios.get('admin/playfair/getTaxes?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getTaxes' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createTax(context,payload){
			axios.post('/admin/playfair/createTax',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
				context.commit('createTax', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeTax(context, id){
			axios.delete('/admin/playfair/tax/'+id)
			.then(response => {
                context.commit('removeTax', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateTax(context, payload){
	        axios.put('/admin/playfair/tax/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateTax', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getTaxes(state,{ list }){
			state.taxes = list.data
			state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createTax(state,newAttribute){
       		state.taxes.unshift(newAttribute);
		},
		removeTax(state, id){
			let index = state.taxes.findIndex(tax => tax.id == id)
			state.taxes.splice(index,1)
		},
		updateTax(state, {id,draft}){
        	let index = state.taxes.findIndex(tax => tax.id == id);
        	state.taxes.splice(index, 1, draft);
		}
	},
	
}