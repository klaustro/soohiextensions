export default {
	state:{
		discounts:[],
	},
	actions:{
		getDiscounts( context ,payload){
			axios.post('admin/playfair/getDiscounts',payload)
			.then(response => {
				context.commit('getDiscounts' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createDiscount(context,payload){
			axios.post('/admin/playfair/createDiscount',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createDiscount', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeDiscount(context, id){
			axios.delete('/admin/playfair/discount/'+id)
			.then(response => {
                context.commit('removeDiscount', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateDiscount(context, payload){
	        axios.put('/admin/playfair/discount/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateDiscount', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getDiscounts(state, { list }){
			return state.discounts = list
		},
		getAllDiscounts(state,{ list }){
			state.allDiscounts = list.data
		  	
		},
		createDiscount(state,newDiscount){
       		state.discounts.unshift(newDiscount);
		},
		removeDiscount(state, id){
			let index = state.discounts.findIndex(discount => discount.id == id)
			state.discounts.splice(index,1)
		},
		updateDiscount(state, {id,draft}){
        	let index = state.discounts.findIndex(discount => discount.id == id);
        	state.discounts.splice(index, 1, draft);
		}	
	},
	
}