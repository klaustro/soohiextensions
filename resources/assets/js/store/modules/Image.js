export default {
	state:{
		images:[],
	},
	actions:{
		getImages(context,payload){
			axios.post('admin/playfair/getImages',payload)
			.then(response => {
				context.commit('getImages' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createImage(context,payload){
			axios.post('/admin/playfair/createImage',payload)
			.then(response => {	
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})	
				context.commit('createImages', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeImage(context,id){
			axios.delete('/admin/playfair/image/'+id)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})	
				context.commit('removeImage', id)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateImage(context,payload){
			axios.put('/admin/playfair/image/'+payload.id, payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})	
				context.commit('updateImage', payload)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		}
	},
	mutations:{
		updateImage(state, {id,draft,Base64Img}){
			let index = state.images.findIndex(image => image.id == id);
        	state.images.splice(index, 1, draft);

		},
		getImages(state, {list} ){
			return state.images = list
		},
		createImages(state,newImage){
			newImage.map(image => {
				state.images.push(image);
			})
		},
		removeImage(state,id){
			let index = state.images.findIndex(image => image.id == id);
        	state.images.splice(index, 1);
		}	
	},
	
}