export default {
	state:{
		price_specials:[],
	},
	actions:{
		getPriceSpecials( context ,payload){
			axios.post('admin/playfair/getPriceSpecials',payload)
			.then(response => {
				context.commit('getPriceSpecials' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createPriceSpecial(context,payload){
			axios.post('/admin/playfair/createPriceSpecial',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createPriceSpecial', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removePriceSpecial(context, id){
			axios.delete('/admin/playfair/price_special/'+id)
			.then(response => {
                context.commit('removePriceSpecial', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updatePriceSpecial(context, payload){
	        axios.put('/admin/playfair/price_special/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updatePriceSpecial', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getPriceSpecials(state, { list }){
			return state.price_specials = list
		},
		createPriceSpecial(state,newPriceSpecial){
       		state.price_specials.push(newPriceSpecial);
		},
		removePriceSpecial(state, id){
			let index = state.price_specials.findIndex(price_special => price_special.id == id)
			state.price_specials.splice(index,1)
		},
		updatePriceSpecial(state, {id,draft}){
        	let index = state.price_specials.findIndex(price_special => price_special.id == id);
        	state.price_specials.splice(index, 1, draft);
		}	
	},
	
}