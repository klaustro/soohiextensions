export default {
	state:{
		customers:[],
		perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getCustomers( context ,params){
			axios.get('admin/playfair/getCustomers?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getCustomers' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createCustomer(context,payload){
			axios.post('/admin/playfair/createCustomer',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createCustomer', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeCustomer(context, id){
			axios.delete('/admin/playfair/customer/'+id)
			.then(response => {
                context.commit('removeCustomer', id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateCustomer(context, payload){
	        axios.put('/admin/playfair/customer/' + payload.id, payload)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateCustomer', response.data.data)
            })
            .catch(error => 
{                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	}
	},
	mutations:{
		getCustomers(state,{ list }){
			state.customers = list.data
			state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createCustomer(state,draft){
       		state.customers.unshift(draft);
		},
		removeCustomer(state, id){
			let index = state.customers.findIndex(Customer => Customer.id == id)
			state.customers.splice(index,1)
		},
		updateCustomer(state, draft){
        	let index = state.customers.findIndex(Customer => Customer.id == draft.id);
        	state.customers.splice(index, 1, draft);
		}
	},
	
}