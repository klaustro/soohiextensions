export default {
	state:{
		attributes:[],
		allAttributes:[],
		perPage: null,
	    currentPage: 1,
	    lastPage: null,
	    totalRows: null,
	},
	actions:{
		getAllAttributes(context){
			axios.get('admin/playfair/getAllAttributes')
				.then(response => {
					context.commit('getAttributes' , { list: response.data })
				}).catch(error => {
					console.log(error.data)
				})
		},
		getAttributes( context , params){
			axios.get('admin/playfair/getAttributes?page='+params.page + '&search=' + params.target)
			.then(response => {
				context.commit('getAttributes' , { list: response.data })
			}).catch(error => {
				console.log(error.data)
			})
		},
		createAttribute(context,payload){
			axios.post('/admin/playfair/createAttribute',payload)
			.then(response => {
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'success'})
				context.commit('createAttribute', response.data.data)
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		removeAttribute(context, payload){
			axios.delete('/admin/playfair/attribute/'+payload.id)
			.then(response => {
                context.commit('removeAttribute', payload.id)
				Vue.toasted.show(response.data.message, {icon: 'trash-o', type: 'error'})
			})
			.catch(error => {
				  Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
			})
		},
		updateAttribute(context, payload){
	        axios.put('/admin/playfair/attribute/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateAttribute', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    	},
	},
	mutations:{
		getAttributes(state,{ list }){
			state.attributes = list.data
			state.allAttributes = list
			state.lastPage = list.last_page
	        state.totalRows = list.total
	        state.perPage = list.per_page
		},
		createAttribute(state,newAttribute){
       		state.attributes.unshift(newAttribute);
		},
		removeAttribute(state, id){
			let index = state.attributes.findIndex(attribute => attribute.id == id)
			state.attributes.splice(index,1)
		},
		updateAttribute(state, {id,draft}){
        	let index = state.attributes.findIndex(attribute => attribute.id == id);
        	state.attributes.splice(index, 1, draft);
		}
	},
	
}