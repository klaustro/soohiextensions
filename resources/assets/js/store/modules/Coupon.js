let state = {
    coupons: [],
    perPage: null,
    currentPage: 1,
    lastPage: null,
    totalRows: null,
    list: [],
}

let getters = {
        findCoupon(state){
            return function(id){
                console.log(id)
                let coupon = state.coupons.find(coupon => coupon.id == id)
                return coupon;
            }
        },
}

let actions = {
    getCoupons(context, params){
        axios.get('admin/playfair/coupon?page=' + params.page + '&search=' + params.target + '&orderBy=' + params.orderBy + '&desc=' + params.desc)
            .then(response => {
                context.commit('getCoupons', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    storeCoupon(context, payload){
        axios.post('admin/playfair/coupon', payload)
            .then(response => {
                let newCoupon = {
                    id: response.data.id,
                    token: response.data.token,
                    value: payload.value,
                    type: payload.type,
                }
                Vue.toasted.show(response.data.message, {icon: 'plus', type: 'success'})
                context.commit('storeCoupon', newCoupon)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    updateCoupon(context, payload){
        axios.put('admin/playfair/coupon/' + payload.id, payload.draft)
            .then(response => {
                Vue.toasted.show(response.data.message, {icon: 'pencil', type: 'info'})
                context.commit('updateCoupon', payload)
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },

    removeCoupon(context, id){
        axios.delete('admin/playfair/coupon/' + id)
            .then(response => {
                context.commit('removeCoupon', id)
                Vue.toasted.show(response.data.message, {icon: 'trash', type: 'error'})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })

    },

    listCoupons(context){
        axios.get('admin/playfair/couponList')
            .then(response => {
                context.commit('listCoupons', {data: response.data})
            })
            .catch(error => {
                Vue.toasted.show(error.message, {icon: 'exclamation-triangle', type: 'error'})
            })
    },
}

let mutations = {
    getCoupons(state, {data}){
        state.currentPage = data.current_page
        state.lastPage = data.last_page
        state.totalRows = data.total
        state.perPage = data.per_page
        state.coupons = data.data;
    },

    storeCoupon(state, newCoupon){
        state.coupons.unshift(newCoupon);
    },

    updateCoupon(state, {id, draft}){
        let index = state.coupons.findIndex(coupon => coupon.id == id);
        state.coupons.splice(index, 1, draft);
    },

    removeCoupon(state, id)    {
        let index = state.coupons.findIndex(coupon => coupon.id == id);
        state.coupons.splice(index, 1);
    },

    listCoupons(state, data){
        state.list = data.data;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}