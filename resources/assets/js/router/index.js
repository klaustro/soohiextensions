import Vue from 'vue'
import Router from 'vue-router'
import Category from '../components/categories/Index.vue'
import Index from '../components/Home.vue'
import Manufacturer from '../components/manufacturers/Index.vue'
import Product from '../components/products/Index.vue'
import createProduct from '../components/products/Cu.vue'
import Inventory from '../components/inventories/Index.vue'
import Attribute from '../components/attributes/Index.vue'
import Customers from '../components/customers/Index.vue'
import CustomersGroup from '../components/customers_group/Index.vue'
import Coupon from '../components/coupons/Index.vue'
import Tax from '../components/taxes/Index.vue'
import Order from '../components/orders/Index.vue'
import Discount from '../components/discount/Cu.vue'
import Setting from '../components/settings/Index.vue'




Vue.use(Router)

let router = new Router({
	mode: 'history',
	routes: [
		{
			name: 'home',
			path: '/home',
			component: Index
		},
		{
			path: '/categories',
			component: Category
		},
		{
			path: '/manufacturers',
			component: Manufacturer
		},
		{
			path: '/products',
			name: 'products',
			component: Product
		},
		{
			path: '/cu-products',
			name:'createProduct',
			component: createProduct,
			props:true,
		},
		{
			path: '/inventories',
			component: Inventory
		},
		{
			path: '/attributes',
			component: Attribute
		},
		{
			path: '/discount',
			component: Discount
		},
		{
			path: '/customers_group',
			component: CustomersGroup
		},
		{
			path: '/customers',
			component: Customers
		},
		{
			path: '/coupons',
			component: Coupon
		},
		{
			path: '/taxes',
			component: Tax
		},
		{
			path: '/orders',
			component: Order
		},
		{
			name: 'settings',
			path: '/settings',
			component: Setting
		},
		{
			path: '*',
			component:
			{
				template : '<h1>Error 404</h1>'
			}
		},

	]
})


export default router