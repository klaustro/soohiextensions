import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import wysiwyg from "vue-wysiwyg"
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Toasted from 'vue-toasted'
import VueGallery from 'vue-gallery'
import _ from 'lodash'
import VueSweetalert2 from 'vue-sweetalert2';
import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap')
window.Vue = require('vue')

//*Bootstrap Vue
Vue.use(BootstrapVue)

//*Toasted
Vue.use(Toasted,  {duration: 3000, iconPack : 'fontawesome', theme: 'outline'})

//*Wysiwyg
Vue.use(wysiwyg, {  maxHeight: "500px" })

//*Vue gallery
Vue.component('vueGallery', VueGallery);

//*Moment
Vue.use(require('vue-moment'))

//*flat pickr
Vue.use(flatPickr);

//Swal alert
Vue.use(VueSweetalert2)

//Vue Form Wizard
Vue.use(VueFormWizard)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component',require('./components/ExampleComponent.vue'));
Vue.component('cu-category', require('./components/categories/Cu.vue'));
Vue.component('cu-manufacturer', require('./components/manufacturers/Cu.vue'));
Vue.component('cu-product', require('./components/products/Cu.vue'));
Vue.component('cu-comparison', require('./components/comparisons/Cu.vue'));
Vue.component('view-general', require('./components/general/Index.vue'));
Vue.component('view-data', require('./components/data/Index.vue'));
Vue.component('view-image', require('./components/image/Index.vue'));
Vue.component('view-attribute', require('./components/attributes/Index.vue'));
Vue.component('view-comparison', require('./components/comparisons/Index.vue'));
Vue.component('c-attribute', require('./components/attributes/C.vue'));
Vue.component('view-attribute-value', require('./components/attribute-values/AttributeValue.vue'));
Vue.component('c-attribute-value', require('./components/attribute-values/C.vue'));
Vue.component('view-option', require('./components/options/Index.vue'));
Vue.component('c-option', require('./components/options/Cu.vue'));
Vue.component('cu-customer-group', require('./components/customers_group/Cu.vue'));
Vue.component('cu-customer', require('./components/customers/Cu.vue'));
Vue.component('coupon-edit', require('./components/coupons/Edit.vue'));
Vue.component('cu-tax', require('./components/taxes/Cu.vue'));
Vue.component('cu-order', require('./components/orders/Cu.vue'));
Vue.component('cu-discount', require('./components/discount/Cu.vue'));
Vue.component('cu-price-special', require('./components/price_specials/Cu.vue'));
Vue.component('cu-setting', require('./components/settings/Cu.vue'))

//Localization
Vue.prototype.trans = string => _.get(window.i18n, string)
window.trans = string => _.get(window.i18n, string)

// clone
window.clone = function (obj) {
    return JSON.parse(JSON.stringify(obj))
}

Vue.filter('money', function(value){
    var numeral = require('numeral')
    return numeral(value).format('0,0.00')
})


const app = new Vue({
    el: '#app',
    router,
    store,
})
