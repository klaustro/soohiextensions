@extends('adminlte::master')

@section('adminlte_css')
<link rel="stylesheet"
href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'green') . '.min.css')}} ">
@stack('css')
@yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'green') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
    ][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

    @section('body')
    <div class="wrapper" id="app">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top navbar-size">
                <div class="container">
                    <div class="navbar-header">
                        <router-link to="home" tag="a">
                            <img src="{{asset('storage/logo/logo.png')}}" alt="logo">

                        </router-link>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                    @else
                    <!-- Logo -->
                    <router-link to="home" tag="a" class="logo">
                       <img src="{{asset('storage/logo/logo.png')}}" alt="logo">
                    </router-link>

                    <!-- Header Navbar -->
                    <nav class="navbar navbar-static-top" role="navigation">
                        <!-- Sidebar toggle button-->
                        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                            <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                        </a>
                        @endif
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">

                            <ul class="nav navbar-nav">
                                <li>
                                    @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                    <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                        <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                    </a>
                                    @else
                                    <a href="#"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                    >
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                    {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                                @endif
                            </li>
                        </ul>
                    </div>
                    @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        <!-- Navbar -->
        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar" style="height: auto;">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <br>
                    </div>
                    <div class="pull-left info">
                        @if(Auth::check())
                        <p>{{Auth()->user()->name}}</p>
                        @else
                            <script>window.location = "/";</script>
                        @endif
                    </div>
                </div>

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu tree" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i>
                        <span>Catalogs</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">

                        <router-link tag="li" to="/categories" >
                            <a><i class="fa fa-circle-o"></i> Categories</a>
                        </router-link>

                        <router-link tag="li" to="/manufacturers" >
                            <a><i class="fa fa-circle-o"></i> Manufacturers</a>
                        </router-link>

                        <router-link tag="li" to="/products" >
                            <a><i class="fa fa-circle-o"></i> Products</a>
                        </router-link>

                        <router-link tag="li" to="/inventories" >
                            <a><i class="fa fa-circle-o"></i> Inventory </a>
                        </router-link>

                        <router-link tag="li" to="/attributes" >
                            <a><i class="fa fa-circle-o"></i> Attributes </a>
                        </router-link>
                    </ul>
                </li>

                 <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i>
                        <span>Sales</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <router-link tag="li" to="/orders" >
                            <a><i class="fa fa-circle-o"></i> Orders </a>
                        </router-link >

                        <router-link tag="li" to="/customers" >
                            <a><i class="fa fa-circle-o"></i> Customers </a>
                        </router-link>

                        <router-link tag="li" to="/customers_group" >
                            <a><i class="fa fa-circle-o"></i> Customers Group</a>
                        </router-link>

                        <router-link tag="li" to="/coupons" >
                            <a><i class="fa fa-circle-o"></i> Coupons </a>
                        </router-link >
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th"></i>
                        <span>Systems</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <router-link tag="li" to="/settings" >
                            <a><i class="fa fa-circle-o"></i> {{trans('app.setting.title')}} </a>
                        </router-link >

                         <router-link tag="li" to="/taxes" >
                            <a><i class="fa fa-circle-o"></i> Taxes </a>
                        </router-link >
                    </ul>

                </li>
            </section>
        <!-- /.sidebar -->
        </aside>
        @endif
        <!-- /.fin navbar -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    @if(config('adminlte.layout') == 'top-nav')
    <div class="container">
        @endif

        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('content_header')
        </section>

        <!-- Main content -->
        <section class="content">

            @yield('content')

        </section>
        <!-- /.content -->
        @if(config('adminlte.layout') == 'top-nav')
    </div>
    <!-- /.container -->
    @endif
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="text-right">
        <p>Copyright © 2018 NT Of America. All Rights Reserved.</p>
        <p>Powered by <a href="http://www.nissienterprise.com/">Nissi Technology Enterprises Inc</a></p>
    </div>
  </footer>
</div>
<!-- ./wrapper -->
@stop

@section('adminlte_js')
<script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
@stack('js')
@yield('js')
@stop
