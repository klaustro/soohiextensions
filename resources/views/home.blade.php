@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content')
    <router-view></router-view>
@stop