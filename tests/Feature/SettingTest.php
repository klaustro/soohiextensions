<?php

namespace Tests\Feature;

use App\Setting;
use App\SettingGroup;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SettingTest extends TestCase
{
    use RefreshDatabase;

    protected $user, $setting_group;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->setting_group = factory(SettingGroup::class)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_setting_is_listed()
    {
        $settings = factory(Setting::class, 20)->create([
            'setting_group_id' => $this->setting_group->id
        ]);

        $this->get('admin/playfair/setting')
             ->assertSuccessful()
             ->assertSee($this->setting_group->name)
             ->assertSee($settings[0]->param)
             ->assertSee($settings[0]->value);

    }

    /** @test */
    function if_setting_can_be_stored()
    {
        $response = $this->post('admin/playfair/setting', [
            'setting_group_id' => $this->setting_group->id,
            'param' => 'param1',
            'value' => 10,
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.setting.store_message')]);

        $this->assertDatabaseHas('settings',[
            'setting_group_id' => $this->setting_group->id,
            'param' => 'param1',
            'value' => 10,
        ]);

    }
    /** @test */
    function if_setting_can_be_updated()
    {
        $setting = factory(Setting::class)->create();

        $this->put('admin/playfair/setting/' . $setting->id, [
            'setting_group_id' => $this->setting_group->id,
            'param' => 'param1',
            'value' => 10,
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.setting.update_message')]);

        $this->assertDatabaseHas('settings',[
            'setting_group_id' => $this->setting_group->id,
            'param' => 'param1',
            'value' => 10,
        ]);

    }
    /** @test */
    function if_setting_can_be_deleted()
    {
        $setting = factory(Setting::class)->create();

        $this->delete('admin/playfair/setting/' . $setting->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.setting.delete_message')]);

        $this->assertDatabaseMissing('settings',[
            'setting_group_id' => $setting->setting_group_id,
            'param' => $setting->param,
            'value' => $setting->value,
        ]);

    }
}
