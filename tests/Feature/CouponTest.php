<?php

namespace Tests\Feature;

use App\Coupon;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CouponTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);
    }

    /** @test */
    function if_coupon_is_paginated()
    {
        $coupons = factory(Coupon::class, 20)->create();

        $this->get('admin/playfair/coupon?page=1&search=&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($coupons[19]->token)
             ->assertDontSee($coupons[0]->token);

    }

    /** @test */
    function if_coupon_is_finded()
    {
        $searched = factory(Coupon::class)->create([
            'token' => 'Searched coupon',
        ]);

        $coupon = factory(Coupon::class)->create();

        $this->get('admin/playfair/coupon?search=Searched&orderBy=id&desc=true')
             ->assertSuccessful()
             ->assertSee($searched->token)
             ->assertDontSee($coupon->token);

    }
    /** @test */
    function if_coupon_can_be_stored()
    {
        $response = $this->post('admin/playfair/coupon', [
            'value' => 10,
            'type' => 'percent',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.coupon.store_message')]);

        $this->assertDatabaseHas('coupons',[
            'value' => 10,
            'type' => 'percent',
        ]);

    }
    /** @test */
    function if_coupon_can_be_updated()
    {
        $coupon = factory(Coupon::class)->create();

        $this->put('admin/playfair/coupon/' . $coupon->id, [
            'value' => 10,
            'type' => 'percent',
        ])
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.coupon.update_message')]);

        $this->assertDatabaseHas('coupons',[
            'value' => 10,
            'type' => 'percent',
        ]);

    }
    /** @test */
    function if_coupon_can_be_deleted()
    {
        $coupon = factory(Coupon::class)->create();

        $this->delete('admin/playfair/coupon/' . $coupon->id)
            ->assertSuccessful()
            ->assertJson(['message' => trans('app.coupon.delete_message')]);

        $this->assertDatabaseMissing('coupons',[
            'token' => $coupon->token,
        ]);

    }
    /** @test */
    function if_coupon_is_ordered_by_any_column()
    {
        $first = factory(Coupon::class)->create([
            'token' => 'AAAAAAAAAAAAAAAAA',
            'value' => 1,
            'type' => 'discount',
        ]);


        $coupons = factory(Coupon::class, 20)->create([
            'token' => 'BBBBBBBBBBBBBBBBBB',
            'value' => 5,
            'type' => 'percent',
        ]);

        $last = factory(Coupon::class)->create([
            'token' => 'ZZZZZZZZZZZZZZZ',
            'value' => 10,
            'type' => 'percent',
        ]);

        $this->get('admin/playfair/coupon?orderBy=token&desc=false')
             ->assertSuccessful()
             ->assertSee($first->token)
             ->assertDontSee($last->token);

        $this->get('admin/playfair/coupon?orderBy=token&desc=true')
             ->assertSuccessful()
             ->assertSee($last->token)
             ->assertDontSee($first->token);

        $this->get('admin/playfair/coupon?orderBy=value&desc=false')
             ->assertSuccessful()
             ->assertSee($first->token)
             ->assertDontSee($last->token);

        $this->get('admin/playfair/coupon?orderBy=value&desc=true')
             ->assertSuccessful()
             ->assertSee($last->token)
             ->assertDontSee($first->token);
    }
}
